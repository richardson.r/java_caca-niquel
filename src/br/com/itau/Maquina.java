package br.com.itau;

import java.util.Arrays;

public class Maquina {

    private Slot[] slots;

    public Maquina() {
        this.slots = new Slot[3];

        for (int i = 0; i < this.slots.length; i++) {
            this.slots[i] = new Slot();
        }
    }

    public int calcularPontuacao() {
        int pontuacao = 0;

        for (Slot slot : slots)
            pontuacao += slot.getItem().pontos;

        if (possuiBonus())
            pontuacao *= 100;

        return pontuacao;
    }

    private boolean possuiBonus() {
        boolean iguais = slots[0].getItem() == slots[1].getItem() && slots[0].getItem() == slots[2].getItem();
        return iguais;
    }

    @Override
    public String toString() {
        return Arrays.toString(slots);
    }
}
