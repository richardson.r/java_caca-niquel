package br.com.itau;

import java.util.Random;

public class Slot {
    private Item item;

    public Slot() {
        int numeroAletorio = new Random().nextInt(Item.values().length);
        this.item = Item.values()[numeroAletorio];
    }

    public Item getItem() {
        return item;
    }

    @Override
    public String toString() {
        return item + " [" + item.pontos + "]";
    }
}
