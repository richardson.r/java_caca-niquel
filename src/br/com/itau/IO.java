package br.com.itau;

import java.util.Scanner;

public class IO {

    public static void inicio() {
        System.out.println("*******************************************");
        System.out.println("* * * * * * Caça Níquel Honesto * * * * * *");
        System.out.println("*******************************************\n");

        Maquina maquina = new Maquina();
        System.out.println(maquina);

        int pontuacao = maquina.calcularPontuacao();

        System.out.println("Você fez " + pontuacao + " Pontos.\n");

        System.out.println("Jogar novamente? (s/n)");
        if(scanner().nextLine().equals("s"))
            inicio();
    }

    private static Scanner scanner() {
        return new Scanner(System.in);
    }
}
