package br.com.itau;

public enum Item {
    BANANA(10),
    FRAMBOESA(50),
    MOEDA(100),
    SETE(300);

    public int pontos;

    Item(int pontos) {
        this.pontos = pontos;
    }
}
